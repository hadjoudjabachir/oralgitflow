package comptes;

public class CompteBancaire {
	private final int numeroCompte;
	protected float solde;

	public CompteBancaire(int numeroCompte, float solde) {
		this.numeroCompte = numeroCompte;
		this.solde = solde;
	}

	public int getNumeroCompte() {
		return numeroCompte;
	}

	public float getSolde() {
		return solde;
	}	
	
	protected void setSolde(float newSolde) {
		this.solde=newSolde;
	}

	public float crediter(float montant) {
		if (montant<0) {
			System.out.println("ERREUR, le montant à créditer doit être positif");
		}else {
			solde+=montant;
		}
		return solde;
	}
	public void crediter2(float montant){
		if (montant<0) {
			throw new IllegalArgumentException("ERREUR, le montant à créditer doit être positif");
		}
		else {
			solde+=montant;
		}
	}
	public float debiter(float montant) {
		if (montant<0) {
			System.out.println("ERREUR, le montant à débiter doit être positif");
		}else if (solde<montant) 
		{
			System.out.println("ERREUR, le solde est suffisant");
		}else {
			solde-=montant;
		}
		return solde;
	}

}
